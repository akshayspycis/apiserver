'use strict';
const mongodb = require('mongodb');
const moment = require('moment');

module.exports = function (Model, options) {

  var ISODateString = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;

  /*!
* Convert the id to be a BSON ObjectID if it is compatible
* @param {*} id The id value
* @returns {ObjectID}
*/
  function ObjectID(id) {
    var match;
    if (id instanceof mongodb.ObjectID) {
      return id;
    }
    if (typeof id !== 'string') {
      return id;
    }
    try {
      // MongoDB's ObjectID constructor accepts number, 12-byte string or 24-byte
      // hex string. For LoopBack, we only allow 24-byte hex string, but 12-byte
      // string such as 'line-by-line' should be kept as string
      if (/^[0-9a-fA-F]{24}$/.test(id)) {
        return new mongodb.ObjectID(id);
      }
      if (match = id.match(ISODateString)) {
        var milliseconds = Date.parse(match[0])
        if (!isNaN(milliseconds)) {
          return new Date(milliseconds);
        }
      }
      if (moment(id, "YYYY-MM-DDTHH:mm:ssZ", true).isValid()
        || moment(id, "YYYY-MM-DDTHH:mm:ssZZ", true).isValid()
        || moment(id, "YYYY-MM-DDTHH:mm:ss.SSSZ", true).isValid()
        || moment(id, "YYYY-MM-DDTHH:mm:ss.SSSZZ", true).isValid()) {
        return moment(id).toDate();
      }

      return id;

    } catch (e) {
      return id;
    }
  }

  /*
     This function converts MongoDB ids to BSON Ids
     * */
  function convertObjectIds(input) {
    // Ignore things that aren't objects.
    if (typeof input !== "object") return input;
    for (var key in input) {
      if (!input.hasOwnProperty(key)) continue;
      var value = input[key];
      var match;
      // Check for string properties which look like dates.
      if (typeof value === "string") {
        input[key] = ObjectID(value);
      } else if (typeof value === "object") {
        // Recurse into object
        convertObjectIds(value);
      }
    }
  }
  Model.observe('before save', function event(ctx, next) {
    var newrecord;
    var currentrecord;
    var partialrecord;

    if (ctx.isNewInstance) {
      convertObjectIds(ctx.instance);
    }
    else if (ctx.data) {
      try {
        partialrecord = JSON.parse(JSON.stringify(ctx.data));
      } catch (error) {
        console.log('ERROR !!! - JSON Parse - objectId - E001');
        console.log(error);
      }

      if (ctx.currentInstance) {
        try {
          currentrecord = JSON.parse(JSON.stringify(ctx.currentInstance));
        } catch (error) {
          console.log('ERROR !!! - JSON Parse - objectId - E002');
          console.log(error);
        }

        newrecord = Object.assign(currentrecord, partialrecord);
        //We are doing a shallow copy currently , ideally this should be a deep copy
        for (var key in partialrecord) {
          if (!partialrecord.hasOwnProperty(key)) continue;
          currentrecord[key] = partialrecord[key];
        }
        //Set the updated current record to the new record
        newrecord = currentrecord;
        convertObjectIds(ctx.currentInstance);
      } else {
        newrecord = partialrecord;
      }
      convertObjectIds(newrecord);
      ctx.data = newrecord;
    }

    next();
  });
};