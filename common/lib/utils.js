'use strict';
var mongodb = require('mongodb');

module.exports = {

  /**
   * This method identifies the child entities and adds them to the parents
   */
  convertDates: convertDates,
  checkDate: checkDate,
  ObjectIdCheck: ObjectIdCheck,
  convertObjectIdsandDates: convertObjectIdsandDates,
  checkObjectIdandDate: checkObjectIdandDate,
  replaceAll: replaceAll,
  escapeRegExp: escapeRegExp
}
/*
Given a key , this function goes deep inside a json object to retrieve the values for the key
*/




function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(str) {
  return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}


/*!
 * Convert the id to be a BSON ObjectID if it is compatible
 * @param {*} id The id value
 * @returns {ObjectID}
 */
function checkDate(id) {
  var match;
  var ISODateString = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;


  if (typeof id !== 'string') {
    return id;
  }
  try {
    // MongoDB's ObjectID constructor accepts number, 12-byte string or 24-byte
    // hex string. For LoopBack, we only allow 24-byte hex string, but 12-byte
    // string such as 'line-by-line' should be kept as string      
    if (match = id.match(ISODateString)) {
      var milliseconds = Date.parse(match[0])
      if (!isNaN(milliseconds)) {
        return new Date(milliseconds);
      }
    }

    return id;

  } catch (e) {
    return id;
  }
}

/*
   This function converts dates to Mongo DB Format
   * */
function convertDates(input) {
  // Ignore things that aren't objects.
  if (typeof input !== "object") return input;
  for (var key in input) {
    if (!input.hasOwnProperty(key)) continue;
    var value = input[key];
    var match;
    // Check for string properties which look like dates.
    if (typeof value === "string") {
      input[key] = checkDate(value);
    } else if (typeof value === "object") {
      // Recurse into object
      convertDates(value);
    }
  }
}

function convertObjectIdsandDates(input) {
  // Ignore things that aren't objects.
  if (typeof input !== "object") return input;
  for (var key in input) {
    if (!input.hasOwnProperty(key)) continue;
    var value = input[key];
    var match;
    // Check for string properties which look like dates.
    if (typeof value === "string") {
      input[key] = checkObjectIdandDate(value);
    } else if (typeof value === "object") {
      // Recurse into object
      convertObjectIdsandDates(value);
    }
  }
}

function checkObjectIdandDate(id) {
  var match;
  var ISODateString = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;

  if (id instanceof mongodb.ObjectID) {
    return id;
  }
  if (typeof id !== 'string') {
    return id;
  }
  try {
    // MongoDB's ObjectID constructor accepts number, 12-byte string or 24-byte
    // hex string. For LoopBack, we only allow 24-byte hex string, but 12-byte
    // string such as 'line-by-line' should be kept as string
    if (/^[0-9a-fA-F]{24}$/.test(id)) {
      return new mongodb.ObjectID(id);
    }
    if (match = id.match(ISODateString)) {
      var milliseconds = Date.parse(match[0])
      if (!isNaN(milliseconds)) {
        return new Date(milliseconds);
      }
    }

    return id;

  } catch (e) {
    return id;
  }
}

function ObjectIdCheck(id) {
  if (id instanceof mongodb.ObjectID) {
    return true;
  }
  if (typeof id !== 'string') {
    return false;
  }
  try {
    // MongoDB's ObjectID constructor accepts number, 12-byte string or 24-byte
    // hex string. For LoopBack, we only allow 24-byte hex string, but 12-byte
    // string such as 'line-by-line' should be kept as string
    if (/^[0-9a-fA-F]{24}$/.test(id)) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    return false;
  }
}
