var Aggregate, debug;
var utils = require('../../common/lib/utils');

Aggregate = require('./query');
var mongodb = require('mongodb');
var ObjectId = require('mongodb').ObjectId;
var app = require('../../server/server');
var loopbackUtils = require('loopback/lib/utils'); // handle both callbacks and promise based invocations

debug = require('debug')('loopback:mixins:aggregate');

module.exports = function(Model) {
    var rewriteId;
    rewriteId = function(doc) {
        if (doc == null) {
            doc = {};
        }
        if (doc._id) {
            doc.id = doc._id;
        }
        delete doc._id;
        return doc;
    };


    Model.aggregate = function(filter, options, callback) {
        callback = callback || loopbackUtils.createPromiseCallback(); // handle both callbacks and promise based invocations
        var aggregate, collection, connector, cursor, model, where, pipeline;
        connector = this.getConnector();
        model = Model.modelName;

        debug('aggregate', model);

        if (!filter.aggregate && !filter.pipeline) {
            return callback(new Error('no aggregate filter or pipeline present'));
        }
        collection = connector.collection(model);

        //Handle dates
        utils.convertObjectIdsandDates(filter);

        /*
        If there is a dataelement id that is passed , then
        1) get the pipeline json from the dataelement
        2) Replace the variables in the filter
        3) Execute the pipeline
        */

        if (filter.pipeline) {
            aggregate = new Aggregate();
            aggregate.pipeline = filter.pipeline;
        } else {
            aggregate = new Aggregate(filter.aggregate);

            if (filter.fields) {
                aggregate.project(filter.fields);
            }
            if (filter.where) {
                where = connector.buildWhere(model, filter.where);
                aggregate.pipeline.unshift({
                    '$match': where
                });
            }
            debug('all.aggregate', aggregate.pipeline);

            if (filter.sort) {
                aggregate.sort(connector.buildSort(filter.sort));
            }
        }

        cursor = aggregate.exec(collection);

        if (filter.limit) {
            cursor.limit(filter.limit);
        }
        if (filter.skip) {
            cursor.skip(filter.skip);
        } else if (filter.offset) {
            cursor.skip(filter.offset);
        }
        // return cursor.toArray().map(rewriteId(data));

        return cursor.toArray(function(err, data) {
            debug('aggregate', model, filter, err, data);
            if (data)
                return callback(err, data.map(rewriteId));
            else
                return callback(err, null);
        });
        // return callback.promise; // handle both callbacks and promise based invocations

    };

    Model.remoteMethod('aggregate', {
        accepts: [{
            arg: "filter",
            description: "Filter defining fields, where, aggregate, order, offset, and limit. Alternate is to provide the entire pipeline as pipeline",
            type: "object"
        }, {
            arg: "options",
            description: "options",
            type: "object"
        }],
        accessType: "READ",
        description: "Find all instances of the model matched by filter from the data source.",
        http: {
            path: "/aggregate",
            verb: "get"
        },
        returns: {
            arg: "data",
            root: true,
            type: 'array'
        }
    });
};