var ObjectID = require('mongodb').ObjectID;
var _ = require('lodash');
var moment =require('moment');
module.exports = function (app) {
  var router = app.loopback.Router();
  const Restaurant = app.models.Restaurant;
  const Menu = app.models.Menu;
  router.get('/ping', function (req, res) {
    res.send('Replay from localhost:3000');
  });
  app.on('started', function () {
    console.log("Express api start")
  });

  app.get('/api/restaurants', async function (req, res, next) {
    const { params, query, body } = req;
    const { textsearch, currenttime } = query;
    let _r=[];
    let restaurant = [];
    let _rm = [];
    try {
      if (currenttime) {
        restaurant =await Restaurant.find({});
        if (restaurant && restaurant.length) {
          restaurant = restaurant.filter(r => {
            if (r.operating_hours && r.operating_hours.length > 0) {
              for (let o of r.operating_hours) {
                if (o.day.toLowerCase() == moment(currenttime).format('dddd').toLowerCase() && moment(currenttime).isBetween(moment(o.from, 'HH:mm'), moment(o.to, 'HH:mm'))) {
                  return true;
                }
              }
            }
          })
        }
      }
      
      if (textsearch) {
        let filter = [
          {
            $match: { $or: [] }
          }
        ];
        filter[0].$match.$or.push({ name: new RegExp(textsearch, "i") });
        filter[0].$match.$or.push({ adderess: new RegExp(textsearch, "i") });
        _r = await promisifiedAggregate(Restaurant, filter);
        let _filter = [
          {
            $match: {
              "name": new RegExp(textsearch, "i")
            }
          }, {
            $lookup: {
              from: "restaurant",
              localField: "restaurantId",
              foreignField: "_id",
              as: "restaurant"
            }
          }, {
            $unwind: {
              path: "$restaurant"
            }
          },
          { $replaceRoot: { newRoot: "$restaurant" } },
          {
            $group:
            {
              _id: { projectId: "_id" },
              restaurant: { $addToSet: "$$ROOT" },
            }
          },
          {
            $unwind: {
              path: "$restaurant"
            }
          },
          { $replaceRoot: { newRoot: "$restaurant" } },
        ]
        _rm = await promisifiedAggregate(Menu, _filter);
      }
      let records = [..._r, ...restaurant,..._rm];
      _.uniqBy(records, function (e) {
        return e.id;
      });
      res.json(records);
    } catch (error) {
      res.json([]);
    }
   
  })

  app.get('/api/restaurants/ratings', async function (req, res, next) {
    const { params, query, body } = req;
    let filter = [
      {
        $group: {
          _id: "$name",
          menus: { $addToSet: "$_id" },
        }
      },
      {
        $lookup: {
          from: "menu",
          localField: "menus",
          foreignField: "_id",
          as: "menus"
        }
      },
      {
        $unwind: {
          path: '$menus'
        }
      },
      {
        $lookup: {
          from: "review",
          let: { menuId: "$menus._id", restaurantId: "$restaurantId" },
          pipeline: [
            {
              $match: {
                $and: [
                  { $expr: { $eq: ["$menuId", "$$menuId"] } }
                ]
              }
            },
            {
              $group: {
                _id: { rating: '$rating', menuId: '$menuId' },
                sum: { $sum: 1 },
              }
            },
            {
              $group: {
                _id: '$_id.menuId',
                rating: { $avg: { $multiply: ["$sum", "$_id.rating"] } },

              }
            }
          ],
          as: "review"
        }

      },
      {
        $unwind: {
          path: "$review"
        }
      },
      {
        $addFields: { 'menus.review': '$review' }
      },
      {
        $lookup: {
          from: "restaurant",
          localField: "menus.restaurantId",
          foreignField: "_id",
          as: "restaurant"
        }
      },
      {
        $addFields: { 'menus.restaurant': '$restaurant' }
      },
      {
        $group: {
          "_id": "$_id",
          menus: { $addToSet: "$menus" }
        }
      }
    ]
    res.json(await promisifiedAggregate(Menu, filter));
  });
  app.use(router);
  function promisifiedAggregate(model, pipeline) {
    return new Promise(function (resolve, reject) {
      model.aggregate({ pipeline: pipeline }, null, function (error, content) {
        if (error) reject(error);
        else resolve(content);
      });
    });
  }
}